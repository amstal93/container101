#dev2
#Base Images
FROM tomcat:latest

#Work Directory
WORKDIR /usr/src/myapp

#Copy code
COPY java /usr/src/myapp/java

#build war file
RUN jar -cvf hello.war java/Main.java

#copy war file to webapps
RUN cp hello.war /usr/local/tomcat/webapps/

#cleaning
RUN rm -rf *

#Expose port
EXPOSE 8080

#Run tomcat
CMD ["catalina.sh", "run"]
